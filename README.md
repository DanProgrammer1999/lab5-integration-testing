# Lab5 -- Integration testing

[Google Sheet](https://docs.google.com/spreadsheets/d/1yHCXoEUViKfjZpvk9n3VBuj2NpTk4emsVJLW6ex0fgA/edit#gid=0)

## Parameters

| Parameter        | Possible values               |
| ---------------- | ----------------------------- |
| type             | budget, luxury, nonsense      |
| plan             | fixed_price, minute, nonsense |
| distance         | <=0, >0                       |
| planned distance | <=0, >0                       |
| time             | <=0, >0                       |
| planned time     | <=0, >0                       |
| discount         | yes, no, nonsense             |

## Specs
| Spec              | Value | 
| -                 | -     |
| Budget price      | 29    |
| Luxury price      | 74    |
| Fixed price       | 10    | 
| Allowed deviation | 14%   |
| Discount          | 12%   |

## Found Bugs
The number of bugs here makes this service unusable.
It is possible that I did not cover all of them, but I covered enough to state that the program cannot be used. 
Here are the ones I found:
- Distance is not used in price calculation (only planned distance)
  - Setting distance to < 0  does not generate an error
  - However, it is used to determine if the driver deviated from the plan
  - Setting the distance to a big number (i.e. 1000), and time to 1, the distance is completely ignored (should be `Invalid Request`).
- With the exception of the budget price, all tariffs are wrong
- Considering the actual tariff values, discount for Innopolis residents is also wrong
- When the driver deviates from the plan (more than the threshold), the price used is different from the actual budget price 
- Given the query with distance > 0, and time = 0, the service returns a value, while `Invalid Request` was expected 
- Similarly, given, for example, time = 2 and distance = 100, the service returnes a value, while this is clearly `Invalid Request` 
(modern cars cannot develop the speed of 3000 km/h)

## Real Values
During the tests, I was able to determine the real (false) values used instead of those specified in the spec. You can see them in the [table](https://docs.google.com/spreadsheets/d/1yHCXoEUViKfjZpvk9n3VBuj2NpTk4emsVJLW6ex0fgA/edit?usp=sharing).

## Conclusion
It is impossible to use this service for anything meaningful. Even if we accept the false values, there are still many issues, the most important of which is ignoring distance completely in calculation of price with minute tariff. This leads to a funny situation, where we can set distance to any number, and time to 0, and the price will be 0, while in fact should have been `Invalid Request`. Other important issue is that when switching from fixed price to minute plan, the tariff used is different from (real) minute tariff.  
